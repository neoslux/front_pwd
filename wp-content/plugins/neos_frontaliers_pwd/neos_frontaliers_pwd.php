<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 08/05/2018
 * Time: 12:16
 */

/*
 * Plugin Name: neos-frontaliers password
 * Description: Password management for lesfrontaliers website
 * Version: 1.0
 * Author: Neos
 * Author URI: https://neos.lu
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*********************************************/
/* -------- IMPORTED FROM DRUPAL ----------- */
/*********************************************/

/**
 * The standard log2 number of iterations for password stretching. This should
 * increase by 1 every Drupal version in order to counteract increases in the
 * speed and power of computers available to crack the hashes.
 */
define('DRUPAL_HASH_COUNT', 15);

/**
 * The minimum allowed log2 number of iterations for password stretching.
 */
define('DRUPAL_MIN_HASH_COUNT', 7);

/**
 * The maximum allowed log2 number of iterations for password stretching.
 */
define('DRUPAL_MAX_HASH_COUNT', 30);

/**
 * The expected (and maximum) number of characters in a hashed password.
 */
define('DRUPAL_HASH_LENGTH', 55);

function drupal_random_bytes($count) {

	// $random_state does not use drupal_static as it stores random bytes.
	static $random_state, $bytes, $has_openssl;
	$missing_bytes = $count - strlen($bytes);
	if ($missing_bytes > 0) {

		// PHP versions prior 5.3.4 experienced openssl_random_pseudo_bytes()
		// locking on Windows and rendered it unusable.
		if (!isset($has_openssl)) {
			$has_openssl = version_compare(PHP_VERSION, '5.3.4', '>=') && function_exists('openssl_random_pseudo_bytes');
		}

		// openssl_random_pseudo_bytes() will find entropy in a system-dependent
		// way.
		if ($has_openssl) {
			$bytes .= openssl_random_pseudo_bytes($missing_bytes);
		}
		elseif ($fh = @fopen('/dev/urandom', 'rb')) {

			// PHP only performs buffered reads, so in reality it will always read
			// at least 4096 bytes. Thus, it costs nothing extra to read and store
			// that much so as to speed any additional invocations.
			$bytes .= fread($fh, max(4096, $missing_bytes));
			fclose($fh);
		}

		// If we couldn't get enough entropy, this simple hash-based PRNG will
		// generate a good set of pseudo-random bytes on any system.
		// Note that it may be important that our $random_state is passed
		// through hash() prior to being rolled into $output, that the two hash()
		// invocations are different, and that the extra input into the first one -
		// the microtime() - is prepended rather than appended. This is to avoid
		// directly leaking $random_state via the $output stream, which could
		// allow for trivial prediction of further "random" numbers.
		if (strlen($bytes) < $count) {

			// Initialize on the first call. The contents of $_SERVER includes a mix of
			// user-specific and system information that varies a little with each page.
			if (!isset($random_state)) {
				$random_state = print_r($_SERVER, TRUE);
				if (function_exists('getmypid')) {

					// Further initialize with the somewhat random PHP process ID.
					$random_state .= getmypid();
				}
				$bytes = '';
			}
			do {
				$random_state = hash('sha256', microtime() . mt_rand() . $random_state);
				$bytes .= hash('sha256', mt_rand() . $random_state, TRUE);
			} while (strlen($bytes) < $count);
		}
	}
	$output = substr($bytes, 0, $count);
	$bytes = substr($bytes, $count);
	return $output;
}

/**
 * Returns a string for mapping an int to the corresponding base 64 character.
 */
function _password_itoa64() {
	return './0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
}

/**
 * Encodes bytes into printable base 64 using the *nix standard from crypt().
 *
 * @param $input
 *   The string containing bytes to encode.
 * @param $count
 *   The number of characters (bytes) to encode.
 *
 * @return
 *   Encoded string
 */
function _password_base64_encode($input, $count) {
	$output = '';
	$i = 0;
	$itoa64 = _password_itoa64();
	do {
		$value = ord($input[$i++]);
		$output .= $itoa64[$value & 0x3f];
		if ($i < $count) {
			$value |= ord($input[$i]) << 8;
		}
		$output .= $itoa64[($value >> 6) & 0x3f];
		if ($i++ >= $count) {
			break;
		}
		if ($i < $count) {
			$value |= ord($input[$i]) << 16;
		}
		$output .= $itoa64[($value >> 12) & 0x3f];
		if ($i++ >= $count) {
			break;
		}
		$output .= $itoa64[($value >> 18) & 0x3f];
	} while ($i < $count);

	return $output;
}

/**
 * Generates a random base 64-encoded salt prefixed with settings for the hash.
 *
 * Proper use of salts may defeat a number of attacks, including:
 *  - The ability to try candidate passwords against multiple hashes at once.
 *  - The ability to use pre-hashed lists of candidate passwords.
 *  - The ability to determine whether two users have the same (or different)
 *    password without actually having to guess one of the passwords.
 *
 * @param $count_log2
 *   Integer that determines the number of iterations used in the hashing
 *   process. A larger value is more secure, but takes more time to complete.
 *
 * @return
 *   A 12 character string containing the iteration count and a random salt.
 */
function _password_generate_salt($count_log2) {
	$output = '$S$';
	// Ensure that $count_log2 is within set bounds.
	$count_log2 = _password_enforce_log2_boundaries($count_log2);
	// We encode the final log2 iteration count in base 64.
	$itoa64 = _password_itoa64();
	$output .= $itoa64[$count_log2];
	// 6 bytes is the standard salt for a portable phpass hash.
	$output .= _password_base64_encode(drupal_random_bytes(6), 6);
	return $output;
}

/**
 * Ensures that $count_log2 is within set bounds.
 *
 * @param $count_log2
 *   Integer that determines the number of iterations used in the hashing
 *   process. A larger value is more secure, but takes more time to complete.
 *
 * @return
 *   Integer within set bounds that is closest to $count_log2.
 */
function _password_enforce_log2_boundaries($count_log2) {
	if ($count_log2 < DRUPAL_MIN_HASH_COUNT) {
		return DRUPAL_MIN_HASH_COUNT;
	}
	elseif ($count_log2 > DRUPAL_MAX_HASH_COUNT) {
		return DRUPAL_MAX_HASH_COUNT;
	}

	return (int) $count_log2;
}

/**
 * Parse the log2 iteration count from a stored hash or setting string.
 */
function _password_get_count_log2($setting) {
	$itoa64 = _password_itoa64();
	return strpos($itoa64, $setting[3]);
}
function _password_crypt($algo, $password, $setting) {
	// Prevent DoS attacks by refusing to hash large passwords.
	if (strlen($password) > 512) {
		return FALSE;
	}
	// The first 12 characters of an existing hash are its setting string.
	$setting = substr($setting, 0, 12);

	if ($setting[0] != '$' || $setting[2] != '$') {
		return FALSE;
	}
	$count_log2 = _password_get_count_log2($setting);
	// Hashes may be imported from elsewhere, so we allow != DRUPAL_HASH_COUNT
	if ($count_log2 < DRUPAL_MIN_HASH_COUNT || $count_log2 > DRUPAL_MAX_HASH_COUNT) {
		return FALSE;
	}
	$salt = substr($setting, 4, 8);
	// Hashes must have an 8 character salt.
	if (strlen($salt) != 8) {
		return FALSE;
	}

	// Convert the base 2 logarithm into an integer.
	$count = 1 << $count_log2;

	// We rely on the hash() function being available in PHP 5.2+.
	$hash = hash($algo, $salt . $password, TRUE);
	do {
		$hash = hash($algo, $hash . $password, TRUE);
	} while (--$count);

	$len = strlen($hash);
	$output =  $setting . _password_base64_encode($hash, $len);
	// _password_base64_encode() of a 16 byte MD5 will always be 22 characters.
	// _password_base64_encode() of a 64 byte sha512 will always be 86 characters.
	$expected = 12 + ceil((8 * $len) / 6);
	return (strlen($output) == $expected) ? substr($output, 0, DRUPAL_HASH_LENGTH) : FALSE;
}


/**********************************************/
/* OVERRIDE WORDPRESS PASSWORD CORE FUNCTIONS */
/**********************************************/

if ( !function_exists('wp_hash_password') ) {
	function wp_hash_password($pwd) {
		if (empty($count_log2)) {
			// Use the standard iteration count.
			$setting =  '$S$D18stYiYYUhdPjbEPudoycfg8l7obfiIINg1HQ8IjlX/i5qtekQG';
			$count_log2 = _password_get_count_log2($setting);
		}
	return _password_crypt('sha512', $pwd, _password_generate_salt($count_log2));
	};
};


if ( !function_exists('wp_check_password') ) {
	function wp_check_password($pwd, $hash, $user_id) {
		$pwd_hash = _password_crypt('sha512', $pwd, $hash);
		$check = false;
		if ($pwd_hash == $hash) {
			$check = true;
		}
		return apply_filters( 'check_password', $check, $pwd, $hash, $user_id );
	}
}

/*
$S$DtXPhzyWlfYtf59XDox8uEBognqH1oRqNGMAfMdWbQ0gSK6d78tf
$S$DggdDQ9kLFN68ukXNUIxJlbi8CtVJe.HoS0cbRKitAm1cbm3jXgZ
*/